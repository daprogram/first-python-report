# Алгоритм решения уравнения методом Гаусса-Жордана

systEquat = [[3, 2, 2], [1, 2, 1]]  # Система уравнений
systLen = len(systEquat)  # Длина системы
result = [0, 0]  # Результат решения для двойной системы

# Преобразование методом Гаусса-Джордана
for i in range(systLen):
    if int(systEquat[i][i]) == 0:
        break

    for j in range(i + 1, systLen):
        if i != j:
            ratio = systEquat[j][i] / systEquat[i][i]

            for k in range(systLen + 1):
                systEquat[j][k] = systEquat[j][k] - ratio * systEquat[i][k]
        else:
            break

result[systLen - 1] = systEquat[systLen - 1][systLen] / systEquat[systLen - 1][systLen - 1]  # Первое число

# Нахождение остальных чисел
for i in range(systLen - 2, -1, -1):
    result[i] = systEquat[i][systLen]

    for j in range(i + 1, systLen):
        result[i] = result[i] - systEquat[i][j] * result[j]

    result[i] = result[i] / systEquat[i][i]  # Второе число

print(result)
